# -*- coding: utf-8 -*-
# Part of Kiran Infosoft. See LICENSE file for full copyright and licensing details.
{
    'name': "Customer Repair Order Portal",
    'summary': """ Customer Repair Order Portal""",
    'description': """
Customer Repair Order Portal
Repair Order
Portal
Customer
Customer Repair Order
""",
    "version": "1.0",
    "category": "Tools",
    'author': "Kiran Infosoft",
    "website": "http://www.kiraninfosoft.com",
    'license': 'Other proprietary',
    'price': 19.0,
    'currency': 'EUR',
    'images': ['static/description/logo.png'],
    "depends": [
        'repair',
        'website',
        'portal'
    ],
    "data": [
        'security/ir.model.access.csv',
        'views/website_template.xml',
    ],
    "application": False,
    'installable': True,
}
