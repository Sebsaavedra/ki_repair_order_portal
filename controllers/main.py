# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http, _
from odoo.exceptions import AccessError
from odoo.http import request
from odoo.addons.portal.controllers.portal import \
CustomerPortal, pager as portal_pager, get_records_pager


class CustomerPortal(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(CustomerPortal, self)._prepare_portal_layout_values()
        partner = request.env.user.partner_id

        SaleOrder = request.env['repair.order']
        service_order_count = SaleOrder.search_count([
            ('partner_id', 'child_of', [partner.commercial_partner_id.id]),
        ])

        values.update({
            'repair_order_count': service_order_count,
        })
        return values

    @http.route([
            '/my/repair/orders',
            '/my/repair/orders/page/<int:page>'
        ], type='http', auth="user", website=True)
    def portal_my_repair_orders(self,
            page=1, date_begin=None, date_end=None, sortby=None, **kw):
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id

        RepairOrder = request.env['repair.order']
        domain = [
            ('partner_id', 'child_of', [partner.commercial_partner_id.id])
        ]

        searchbar_sortings = {
            'date': {'label': _('Newest'), 'order': 'create_date desc'},
            'name': {'label': _('Name'), 'order': 'name'},
        }
        if not sortby:
            sortby = 'date'
        order = searchbar_sortings[sortby]['order']

        archive_groups = self._get_archive_groups('repair.order', domain)
        if date_begin and date_end:
            domain += [
                ('create_date', '>', date_begin),
                ('create_date', '<=', date_end)
            ]

        repair_count = RepairOrder.search_count(domain)
        # pager
        pager = portal_pager(
            url="/my/repair/orders",
            url_args={
                'date_begin': date_begin,
                'date_end': date_end,
                'sortby': sortby
            },
            total=repair_count,
            page=page,
            step=self._items_per_page
        )

        # content according to pager and archive selected
        repairs = RepairOrder.search(
            domain, order=order,
            limit=self._items_per_page,
            offset=pager['offset']
        )
        request.session['my_repair_order_history'] = repairs.ids[:100]

        values.update({
            'date': date_begin,
            'date_end': date_end,
            'repair_orders': repairs,
            'page_name': 'repair_order',
            'archive_groups': archive_groups,
            'default_url': '/my/repair/orders',
            'pager': pager,
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby
        })
        return request.render(
            "ki_repair_order_portal.portal_my_repair_orders", values
        )

    @http.route(['/my/repair_order/<int:repair_order_id>'],
        type='http', auth="public", website=True)
    def portal_my_repair_order(self,
            repair_order_id=None, access_token=None, **kw):
        repair_sudo = self._document_check_access(
            'repair.order', repair_order_id, access_token
        )
        values = {
            'repair_order': repair_sudo,
            'page_name': 'repair_order',
        }
        return request.render(
            "ki_repair_order_portal.portal_my_repair_order", values
        )

    @http.route(['/my/repair/orders/pdf/<int:order_id>'],
        type='http', auth="public", website=True)
    def portal_repair_report(self, order_id, access_token=None, **kw):
        try:
            order_sudo = self._document_check_access(
                'repair.order', order_id, access_token
            )
        except AccessError:
            return request.redirect('/my')

        pdf = request.env.ref(
            'repair.action_report_repair_order'
        ).sudo().render_qweb_pdf([order_sudo.id])[0]
        pdfhttpheaders = [
            ('Content-Type', 'application/pdf'),
            ('Content-Length', len(pdf)),
        ]
        return request.make_response(pdf, headers=pdfhttpheaders)
